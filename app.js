const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// routes link
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();
const port = process.env.PORT || 5000;


// middleware

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// main uri
app.use('/', orderRoutes);
app.use('/users', userRoutes);
app.use('/products', productRoutes);

// mongoose connection
mongoose.connect(`mongodb+srv://elaydumpit:admin123@zuitt-batch197.pa6lt4y.mongodb.net/capstone?retryWrites=true&w=majority`,{

  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', () => {
	console.log('Connection Error');
});
db.once('open', () => {
	console.log('Connected to MongoDB!');
});

app.listen(port, () => {
	console.log(`API is now online at port ${port}.`);
});


