const user = require('../models/user');
const product = require('../models/product');

const order = require ('../models/order');


const bcrypt = require('bcrypt');
const auth = require('../auth');


// create a product for admin only
module.exports.addProduct = async (reqBody, userData) => {
	

  return user.findById(userData.userId).then(result => {

	if (userData.isAdmin == true) {
		
    return user.findOne({productName: reqBody.productName}).then(result => {

      if (result !== null) {
       
        return  "A product with the same name already exists ";
      } else{

        let newProduct = new product({
          productName: reqBody.productName,
          productDescription: reqBody.productDescription,
          productPrice: reqBody.productPrice,
          productStocks: reqBody.productStocks,
          productSku:reqBody.productSku,
          productImage:reqBody.productImage
        })
      
        
        return newProduct.save().then((product, error) => {
          
          if(error) {
            return false
          } else {
            return "Hello " + result.name + ", You have successfully added " + reqBody.productName + " in the product list!"// true
          }
      
        })

      }

    });

	} else {
    return "Sorry you are not allowed to add a product"
  }
  
});
};


// Retrieves all active products

module.exports.getAllActiveProduct = () => {
	return product.find({isActive: true}).then(result => {
		return result;
	});
};

// retrieve a single product

module.exports.getProduct = (reqParams) => {
	return product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// update a single product
module.exports.updateProduct = (reqParams, reqBody, data) => {
	if (data) {

    return user.findOne({productName: reqBody.productName}).then(result => {
      if (result !== null) {
       
        return  "A product with the same name already exists ";
      } else{
        let updatedProduct = {
          productName: reqBody.productName,
          productDescription: reqBody.productDescription,
          productPrice: reqBody.productPrice,
          productStocks: reqBody.productStocks,
          productSku:reqBody.productSku,
          productImage:reqBody.productImage
        };
        // Syntax: findByIdAndUpdate(document ID, updatesToBeAppliced)
        return product.findByIdAndUpdate(reqParams.courseId, updatedProduct).then((updatedProduct, error) => {
          if (error) {
            return "please check your details" //false
          } else {
            return "You have successfully updated " + reqBody.productName + " in the product list!"// true
          };
        });
      }
    })
		
	} else {
		return "Sorry you are not allowed to update a product"
	};
};

// archiving a product
module.exports.archiveProduct = (reqParams, reqBody, data) => {
	if (data) {
		let archivedProduct = {
			isActive: reqBody.isActive
		};
		// Syntax: findByIdAndUpdate(document ID, updatesToBeAppliced)
		return product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error) => {
			if (error) {
				return "please check your details" //false;
			} else {
        return "Successfully Archived"  //true;
			};
		});
	} else {
		return "Sorry you are not allowed to archive a product"
	};
};
