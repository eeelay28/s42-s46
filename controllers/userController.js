const user = require('../models/user');
const product = require('../models/product');
const order = require ('../models/order');


const bcrypt = require('bcrypt');
const auth = require('../auth');


//User Registration
module.exports.registerUser = (reqBody) => {

	
	return user.findOne({email: reqBody.email}).then(result => {

		
		if (result !== null) {
		
			return  "A user with the provided email already exists ";

		} else if(reqBody.name == "" || reqBody.email == "" || reqBody.password ==""){

			return "Hello "+ reqBody.name + ", please check you for empty input field/s! "

		}else if (!/^[a-zA-Z ]*$/.test(reqBody.name)){
	
			return "Invalid character in the name fields entered please check!"
	
		}else if (reqBody.email.search(/[@]/i) < 0){
	
			return "Your email format is incorrect! "
	
		}else if (reqBody.password.length < 8){
	
			return "Your password is too short! "
	
		}else if (reqBody.password.search(/[a-z]/i) < 0){
	
			return "Your password must contain at least one letter! "
	
		}else if (reqBody.password.search(/[A-Z]/) < 0){
	
			return "Your password must contain at least one uppercase letter! "
	
		}else if (reqBody.password.search(/[a-z]/) < 0){
	
			return "Your password must contain at least one lowercase letter! "
	
		}else if (reqBody.password.search(/[0-9]/) < 0){
	
			return "Your password must contain at least one number! "
	
		}else if(reqBody.password.search(/[!@#$%^&*() =+_-]/) < 0){
			
			return "Your password must contain at least one symbol! "
		}else {
			
			let newUser = new user({
				name: reqBody.name,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				isAdmin: reqBody.isAdmin, 
			
			})
		
			
			return newUser.save().then((user, error) => {
				
				if(error) {
					return false
				} else {
					return "Hello "+ reqBody.name +", you have successfully registered!"// true
				}
			})
		}
	})
};

		
	
			
		
	


//User Authentication
module.exports.loginUser = (reqBody) => {

	
	return user.findOne({email: reqBody.email}).then(result => {

		
		if (result == null) {
			return "user not found!" //false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				
				return "Welcome " + result.name + ", this is your access token : " + auth.createAccessToken(result); 

			} else {
				return "Sorry " + result.name + ", your password in incorrect!" //false 
			}
		}

	})
};


// Route for viewing user details
module.exports.viewDetails = async (reqBody, userData) => {
	

  return user.findById(userData.userId).then(result => {

	if (userData.isAdmin == true || userData.isAdmin !== true) {

		let output =  "Name: "+ result.name + " Email: " + result.email
		return output;


	}else{

		return "you are not a verified user yet, please sign up!"
	}

});
}