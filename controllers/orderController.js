const user = require('../models/user');
const product = require('../models/product');



const bcrypt = require('bcrypt');
const auth = require('../auth');
const order = require('../models/order');


// placing an order
module.exports.placeOrder = async (reqBody, userData) => {
	

  return user.findById(userData.userId).then(result => {

	if (userData.isAdmin == true) {

    return "an administrator is not allowed to place an order"

      } else{

        return product.findOne({productId: reqBody.productId}).then(result => {

          if ( result !== null){

            let totalAmount = result.productPrice * reqBody.quantity
        
              const orders = new order({
                user_id: user.id,
                productId: reqBody.productId,
                quantity: reqBody.quantity,
                price: result.productPrice,
                image: result.productImage
              });

              return orders.save().then((order, error) => {
          
                if(error) {
                  return "check you details"
                } else {

                  return "You have successfully added " + result.productName + "amounting to " + totalAmount + " in your cart!"// true
                }
            
              })


          }else{
            return "please check your product Id"
          }
        });

        



      }

    })
  }

