const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Route for adding a product.
router.post("/add", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => {
		res.send(resultFromController);
	});
 
});

// Route to retrieving all active products
router.get('/active', (req, res) => {
	productController.getAllActiveProduct().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route for retrieving a single product
router.get('/:productId', (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route for updating a product
router.put('/:productId', auth.verify, (req, res) => {
	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	// console.log(isAdminData);
	productController.updateProduct(req.params, req.body, isAdminData).then(resultFromController => {
		res.send(resultFromController);
	});
});


// Route for archiving a product
router.put('/:productId/archive', auth.verify, (req, res) => {
	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	// console.log(isAdminData);
	productController.archiveProduct(req.params, req.body, isAdminData).then(resultFromController => {
		res.send(resultFromController);
	});
});




module.exports = router;