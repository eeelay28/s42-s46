const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const orderController = require('../controllers/orderController');
const auth = require('../auth');
const userOtpVerification = require('../models/userOtpVerification');

// non admin user check out
router.post("/orders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.placeOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => {
		res.send(resultFromController);
	});
 
});


module.exports = router;