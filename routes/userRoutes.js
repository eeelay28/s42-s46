const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');
const userOtpVerification = require('../models/userOtpVerification');

// Route for user registration
router.post("/register", (req, res) => {
  
  userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user authentication
router.post("/login", (req, res) => {
  userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user details
router.post("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController.viewDetails(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => {
		res.send(resultFromController);
	});
 
});




module.exports = router;