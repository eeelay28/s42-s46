const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({ 
  
  userId: {
		type: String
	},
	
	productId:{
			type: String,
			required: [true, "Product ID is required."]
	},
		
	quantity:{
			type: Number,
			default: 0
			
	},
		
	price:{
			type: Number,
			default: 0
	},

	image:{
			type: String,
			default: null
	},

	cancellationStat: {
		type: Boolean,
		default: false
	},
	totalAmount: {
		type: Number,
		default: 0
	},
  purchasedOn:{
    type: Date,
    default: new Date()
  }

});

module.exports = mongoose.model("order", orderSchema);