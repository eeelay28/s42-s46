const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({ 
  
  name: {
		type: String
		// required: [true, "Firstname is required!"]
	},
	email: {
		type: String
		// required: [true, "Email address is required!"]
	},
	password: {
		type: String
		// required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	verified: {
		type: Boolean,
		default: false
	}


});

module.exports = mongoose.model("user", userSchema);