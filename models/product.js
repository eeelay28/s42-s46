const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({ 
  
  productName: {
		type: String,
		required: [true, "Product Name is required!"]
	},
	productDescription: {
		type: String,
		required: [true, "Product Description is required!"]
	},
	productPrice: {
		type: Number,
		required: [true, "Product Price is required!"]
	},
  productStocks: {
		type: Number,
		required: [true, "Product Stocks is required!"]
	},
  productSku: {
		type: String,
		required: [true, "Product SKU is required!"]
	},
	productImage: {
		type: String,
		required: [true, "Product Image is required!"]
	},
  isActive: {
    type: Boolean,
		default: false
	},
  createdOn:{
    type: Date,
    default: new Date()
  }


	


});

module.exports = mongoose.model("product", productSchema);