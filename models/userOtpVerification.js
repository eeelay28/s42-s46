const mongoose = require ("mongoose");


const userOtpVerificationSchema = new mongoose.Schema({

  userId: {
    type: String
  },
  otp: {
    type: String
  },
  createdAt:{
    type: Date,
    default: new Date()
  },
  expiresAt:{
    type: Date,
    default: new Date()
  }

});

module.exports = mongoose.model("userOtpVerification", userOtpVerificationSchema);
